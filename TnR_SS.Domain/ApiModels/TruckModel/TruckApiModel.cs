﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TnR_SS.Domain.ApiModels.TruckModel
{
    public class TruckApiModel
    {
        public int Id { get; set; }
        public string LicensePlate { get; set; }
    }
}
