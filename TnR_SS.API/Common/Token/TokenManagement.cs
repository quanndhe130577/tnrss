﻿using AutoMapper.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;


namespace TnR_SS.API.Common.Token
{
    public class TokenManagement
    {
        public static Microsoft.Extensions.Configuration.IConfiguration AppSetting { get; }
        static TokenManagement()
        {
            AppSetting = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();
        }

        public static string GetTokenUser(int id)
        {
            //create claims details based on the user information

            var now = DateTime.Now;

            /*var claims = new[] {
                    new Claim(JwtRegisteredClaimNames.Sub, Startup.StaticConfig["Jwt:Subject"]),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Iat, now.ToString()),
                    new Claim("ID", id.ToString()),
            };*/

            var claims = new Claim[4];
            claims[0] = new Claim(JwtRegisteredClaimNames.Sub, AppSetting["Jwt:Subject"]);
            claims[0] = new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString());
            claims[0] = new Claim(JwtRegisteredClaimNames.Iat, now.ToString());
            claims[0] = new Claim("ID", id.ToString());




            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AppSetting["Jwt:Key"]));

            var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(AppSetting["Jwt:Issuer"], AppSetting["Jwt:Audience"], claims, expires: now.AddHours(1), signingCredentials: signIn);

            return new JwtSecurityTokenHandler().WriteToken(token);

        }

        public static bool CheckUserIdFromToken(HttpContext _context, int userId)
        {
            var currentUser = _context.User;

            if (currentUser.HasClaim(c => c.Type == "ID"))
            {
                int claimdId = int.Parse(currentUser.Claims.FirstOrDefault(c => c.Type == "ID").Value);
                if (userId == claimdId) return true;
            }

            return false;
        }

        public static int GetUserIdInToken(HttpContext _context)
        {
            var currentUser = _context.User;

            if (currentUser.HasClaim(c => c.Type == "ID"))
            {
                int claimdId = int.Parse(currentUser.Claims.FirstOrDefault(c => c.Type == "ID").Value);
                return claimdId;
            }
            throw new Exception("Access denied");
        }
    }
}
